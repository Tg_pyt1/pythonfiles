import os
import person_data
import pickle

# Tworzymy klase Osoba w ktorej znajduja sie pola (imie, nazwisko, wiek, opis, tablicaObecnosci)
# w inicie trzeba stworzyc obiekty puste
# klasa powinna zawierac tez metody load i save ktore beda zczytywac dane i zapisywac dane
# ex:  load(imie, nazwisko, wiek, opis, tablicaObecnosci)
#     imie, nazwisko, wiek, opis, tablicaObecnosci = save()

# za pierwszym razem tworzymy 4 osoby i zapisujemy do pliku osoby.txt
# ("Damian", "Wojtas", 33, "TO ja Damian", [3,12, 28, 30])
# ("Ania", "Kotek", 55, "TO ja Kotek", [6,1, 3, 16])
# ("Michal", "Ptaku", 12, "TO ja Ptaku", [9,14, 22, 13])
# ("Kasia", "Jana", 39, "TO ja Kasia", [8,12, 22, 19])

#   w funkcji main wczytac dane z plikow do tablicy i podpiac pod klase Osoba (trzeba utworzyc obiekt osoby)
#   przeszukac wszystkie osoby w wieku ponizej 40 i w ich opisie zamienic na %%%
#   zapisac do pliku osoby2.txt


def isFileExists(fileName):
    if os.path.exists(fileName):
        return True
    return False

def init(fileName):
    data = [
        ("Damian", "Wojtas", 33, "TO ja Damian", [3, 12, 28, 30]),
        ("Ania", "Kotek", 55, "TO ja Kotek", [6, 1, 3, 16]),
        ("Michal", "Ptaku", 12, "TO ja Ptaku", [9, 14, 22, 13]),
        ("Kasia", "Jana", 39, "TO ja Kasia", [8, 12, 22, 19])
    ]
    osoby = []
    # person1 = osoba.Osoba()
    # person1.load(data[0][0], data[0][1], data[0][2], data[0][3], data[0][4])
    # osoby.append(person1)
    # person2 = osoba.Osoba()
    # person2.load(data[1][0], data[1][1], data[1][2], data[1][3], data[1][4])
    # osoby.append(person2)
    # person3 = osoba.Osoba()
    # person3.load(data[2][0], data[2][1], data[2][2], data[2][3], data[2][4])
    # osoby.append(person3) itd ...

    for el in data:
        person = person_data.Osoba()
        person.load(el)
        osoby.append(person)

    with open(fileName, "wb") as osoby_file:
        for el in osoby:
            pickle.dump(el.save(), osoby_file)
    print("INIT")

def edit_osoby_data():
    osoby = []
    with open(fileName, "rb") as osoby_file:
        for el in range(0, 4):
            person = person_data.Osoba()
            person.load(pickle.load(osoby_file))
            osoby.append(person)
    # dowod ze po zczytaniu z pliku to te same osoby :)
    osoby = get_osoby(fileName)

    for el in osoby:
        if el.wiek < 40:
            el.opis = "%%%"

    with open("osoby2.txt", "wb") as osoby_file:
        for el in osoby:
            pickle.dump(el.save(), osoby_file)

def get_osoby(fileName):
    osoby = []
    with open(fileName, "rb") as osoby_file:
        for el in range(0, 4):
            person = person_data.Osoba()
            person.load(pickle.load(osoby_file))
            osoby.append(person)
    return osoby

if __name__ == "__main__":
    fileName = "osoby.txt"
    if not isFileExists(fileName):
        init(fileName)
    edit_osoby_data()

    # dowod ze po zmianie wszystko sie zapisalo
    osoby = get_osoby("osoby2.txt")

    for el in osoby:
        print(el.save())