class Osoba:
    def __init__(self):
        self.imie = "XXXXX"
        self.nazwisko = ""
        self.wiek = 0
        self.opis = ""
        self.tablicaObecnosci = []

    def load(self, data):
        self.imie = data[0]
        self.nazwisko = data[1]
        self.wiek = data[2]
        self.opis = data[3]
        self.tablicaObecnosci = data[4]

    def save(self):
        return (self.imie,
        self.nazwisko,
        self.wiek,
        self.opis,
        self.tablicaObecnosci)
