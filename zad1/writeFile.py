owoce = ("banan", "jablko", "gruszka", 'ananas')
#
owoce_file = open("owoce.txt", "w")

print(owoce, file=owoce_file)

owoce_file.close()
#
# owoce_file = open("owoce.txt", "r")
# owoce = eval(owoce_file.read())
# print(owoce)


with open("owoce.txt", "w") as owoce_file:
    print(owoce, file=owoce_file)



# stworzyc program ktory:
#     - przy pierwszym uruchomieniu tworzy plik oraz wpisuje do niego imie i nazwisko
#     - przy kazdym kolejnym uruchomieniu ma dopisywac do istniejacego pliku twoj wiek

# na koncu wypisz zawartosc pliku w konsoli