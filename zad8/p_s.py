import pickle
import os


class Osoba:

    def __init__(self, fileName):
        self.fileName = fileName
        self.load()


    def set_data(self, data):
        self.imie = data[0]
        self.nazwisko = data[1]
        self.wiek = data[2]
        self.opis = data[3]
        self.tablicaObecnosci = data[4]


    def load(self):
        if self.isFileExists():
            with open(self.fileName, "rb") as osoby_file:
                data = pickle.load(osoby_file)
                # print(data[0])
            self.set_data(data)
        else:
            self.set_data(("","",0,"",[]))

    def save(self):
        dane = (self.imie,
        self.nazwisko,
        self.wiek,
        self.opis,
        self.tablicaObecnosci)
        with open(self.fileName, "wb") as osoby_file:
            # print("ZAPIS")
            # print(self.fileName)
            # print(self.imie)
            pickle.dump(dane, osoby_file)


    def isFileExists(self):
        if os.path.exists(self.fileName):
            return True
        return False