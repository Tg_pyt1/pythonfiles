import os
from p_s import Osoba
import pickle

data = [
    ("Damian", "Wojtas", 33, "TO ja Damian", [3, 12, 28, 30]),
    ("Ania", "Kotek", 55, "TO ja Kotek", [6, 1, 3, 16]),
    ("Michal", "Ptaku", 12, "TO ja Ptaku", [9, 14, 22, 13]),
    ("Kasia", "Jana", 39, "TO ja Kasia", [8, 12, 22, 19])
]
lista_osob = [
    ("Damian", "Wojtas"),
    ("Ania", "Kotek"),
    ("Michal", "Ptaku"),
    ("Kasia", "Jana"),
]
def init():

    osoby = []

    for el in data:
        fileName = el[0] + "_" + el[1] + ".txt"
        person = Osoba(fileName) # inicjalizacja klasy Osoba (__init__)
        person.set_data(el) # wczytanie danych z tablicy data
        person.save() # zapis do pliku kazdej osoby osobno
        osoby.append(person)

    for el in data:
        person = Osoba(el[0] + "_" + el[1] + ".txt")
        print("Wczytalem osobe : " + person.imie)

def read_person_table():
    osoby = []
    for el in data:
        person = Osoba(el[0] + "_" + el[1] + ".txt")
        osoby.append(person)
    return osoby

if __name__ == "__main__":
   init()